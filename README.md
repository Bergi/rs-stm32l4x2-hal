# stm32l4x2-hal

[![Crates.io](https://img.shields.io/crates/v/stm32l4x2-hal.svg)](https://crates.io/crates/stm32l4x2-hal)
[![Documentation](https://docs.rs/stm32l4x2-hal/badge.svg)](https://docs.rs/crate/stm32l4x2-hal/)

HAL for STM32L4x2 family of microcontrollers.

