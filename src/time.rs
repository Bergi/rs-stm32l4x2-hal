//!Time related types
macro_rules! impl_struct {
    ($($name:ident,)+) => {
        $(
            #[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
            pub struct $name(pub u32);
            /* impl Into<$name> for u32 {
                fn into(self) -> $name {
                    $name(self)
                }
            } */
            impl ::core::ops::Div<u32> for $name {
                type Output = Self;
                fn div(self, rhs: u32) -> Self {
                    $name(self.0 / rhs)
                }
            }
            impl ::core::ops::Mul<u32> for $name {
                type Output = Self;
                fn mul(self, rhs: u32) -> Self {
                    $name(self.0 * rhs)
                }
            }
        )+
    }
}

impl_struct!(Hertz, KiloHertz, MegaHertz,);

impl Into<Hertz> for KiloHertz {
    fn into(self) -> Hertz {
        Hertz(self.0 * 1_000)
    }
}

impl Into<Hertz> for MegaHertz {
    fn into(self) -> Hertz {
        Hertz(self.0 * 1_000_000)
    }
}

impl Into<KiloHertz> for MegaHertz {
    fn into(self) -> KiloHertz {
        KiloHertz(self.0 * 1_000)
    }
}

impl<I: Into<Hertz>> ::core::ops::Div<I> for Hertz {
    type Output = u32;
    fn div(self, rhs: I) -> u32 {
        self.0 / rhs.into().0
    }
}
macro_rules! impl_ops {
    ($name:ident) => {
        impl<I: Into<Hertz>> ::core::ops::Div<I> for $name {
            type Output = u32;
            fn div(self, rhs: I) -> u32 {
                let lhs: Hertz = self.into();
                lhs / rhs.into()
            }
        }
    }
}
impl_ops!{KiloHertz}
impl_ops!{MegaHertz}