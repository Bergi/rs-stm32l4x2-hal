use ::stm32l4x2;

pub fn enable_dma1(_: stm32l4x2::DMA1, ahb: &mut ::rcc::AHB) -> (DMA1Channel1, DMA1Channel2, DMA1Channel3, DMA1Channel4, DMA1Channel5, DMA1Channel6, DMA1Channel7) {
    ahb.enr1().modify(|_, w| w.set_dma1en());
    (DMA1Channel1, DMA1Channel2, DMA1Channel3, DMA1Channel4, DMA1Channel5, DMA1Channel6, DMA1Channel7)
}

pub trait InterruptStatusR {
    fn transfer_error(&self) -> bool;
    fn half_transfer(&self) -> bool;
    fn transfer_complete(&self) -> bool;
    fn global(&self) -> bool;
}
pub trait InterruptFlagClearW {
    fn transfer_error(&mut self) -> &mut Self;
    fn half_transfer(&mut self) -> &mut Self;
    fn transfer_complete(&mut self) -> &mut Self;
    fn global(&mut self) -> &mut Self;
}
// pub trait ConfigurationRegisterRead {}
pub trait ConfigurationRegisterWrite {
    // mem2mem
    // priority
    // msize
    // psize
    fn minc(&mut self, bool) -> &mut Self;
    // pinc
    fn circ(&mut self, bool) -> &mut Self;
    fn dir(&mut self, bool) -> &mut Self;
    fn teie(&mut self, bool) -> &mut Self;
    fn htie(&mut self, bool) -> &mut Self;
    fn tcie(&mut self, bool) -> &mut Self;
    fn en(&mut self, bool) -> &mut Self;
}
pub trait ConfigurationRegister {
    // type R: ConfigurationRegisterRead;
    type W: ConfigurationRegisterWrite;
    // modify<F>(&self, f: F) where for<'w> F: FnOnce(&Self::R, &'w mut Self::W) -> &'w mut Self::W;
    fn reset(&self);
    fn write<F>(&self, f: F) where F: FnOnce(&mut Self::W) -> &mut Self::W;
    // fn read(&self) -> Self::R;
}
// pub trait NumberOfDataRegister {}
// pub trait PeripheralAddressRegister {}
// pub trait MemoryAddressRegister {}
pub trait Channel {
    type IS: InterruptStatusR;
    type IFC: InterruptFlagClearW;
    type CCR: ConfigurationRegister;
    // type CNDTR: NumberOfDataRegister;
    // type CPARC: PeripheralAddressRegister;
    // type CMARC: MemoryAddressRegister;
    type Selection;
    fn interrupt_status(&self) -> Self::IS;
    fn interrupt_flagclear<F>(&self, f: F) where F: FnOnce(&mut Self::IFC) -> &mut Self::IFC;
    fn configuration(&self) -> &Self::CCR;

    fn write_counter(&self, u16);
    fn counter(&self) -> u16;
    fn write_peripheral_address(&self, *const u8);
    fn write_memory_address(&self, *const u8);
    fn memory_address(&self) -> *const u8;
    fn memory_buffer(&self, slice: &'static [u8]) {
        self.write_memory_address(slice.as_ptr());
        self.write_counter(slice.len() as u16);
    }
    fn select(&self, Self::Selection);
}

macro_rules! impl_channel {
    ($name:ident, $isrname:ident, $fcwname:ident, $CCR:ident, $sel:ident {
        $ptr:path, $ccrX:ident, $cndtrX:ident, $cparX:ident, $cmarX:ident, $cXs:ident, $teifX:ident, $htifX:ident, $tcifX:ident, $gifX:ident, $cteifX:ident, $chtifX:ident, $ctcifX:ident, $cgifX:ident
    }) => {
        pub struct $name;
        pub struct $isrname(stm32l4x2::dma1::isr::R);
        pub struct $fcwname(stm32l4x2::dma1::ifcr::W);

        impl Channel for $name {
            type IS = $isrname;
            type IFC = $fcwname;
            type CCR = stm32l4x2::dma1::$CCR;
            type Selection = $sel;

            fn interrupt_status(&self) -> Self::IS {
                $isrname(unsafe { &*$ptr() }.isr.read())
            }
            fn interrupt_flagclear<F>(&self, f: F) where F: FnOnce(&mut Self::IFC) -> &mut Self::IFC {
                unsafe { &*$ptr() }.ifcr.write(|w| unsafe { (f((w as *mut stm32l4x2::dma1::ifcr::W as *mut $fcwname).as_mut().unwrap()) as *mut $fcwname as *mut stm32l4x2::dma1::ifcr::W).as_mut().unwrap() });
            }
            fn configuration(&self) -> &Self::CCR {
                & unsafe { &*$ptr() }.$ccrX
            }
            fn write_counter(&self, c: u16) {
                unsafe { &*$ptr() }.$cndtrX.write(|w| unsafe { w.ndt(c) });
            }
            fn counter(&self) -> u16 {
                unsafe { &*$ptr() }.$cndtrX.read().ndt()
            }
            fn write_peripheral_address(&self, p: *const u8) {
                unsafe { &*$ptr() }.$cparX.write(|w| unsafe { w.bits(p as u32) })
            }
            fn write_memory_address(&self, p: *const u8) {
                unsafe { &*$ptr() }.$cmarX.write(|w| unsafe { w.bits(p as u32) })
            }
            fn memory_address(&self) -> *const u8 {
                unsafe { &*$ptr() }.$cmarX.read().bits() as *const u8
            }
            fn select(&self, s: Self::Selection) {
                unsafe { &*$ptr() }.cselr.modify(|_, w| unsafe { w.$cXs(s as u8) })
            }
        }
        impl InterruptStatusR for $isrname {
            fn transfer_error(&self) -> bool { (self.0).$teifX() }
            fn half_transfer(&self) -> bool { (self.0).$htifX() }
            fn transfer_complete(&self) -> bool { (self.0).$tcifX() }
            fn global(&self) -> bool { (self.0).$gifX() }
        }
        impl InterruptFlagClearW for $fcwname {
            fn transfer_error(&mut self) -> &mut Self { (self.0).$cteifX(); self }
            fn half_transfer(&mut self) -> &mut Self { (self.0).$chtifX(); self }
            fn transfer_complete(&mut self) -> &mut Self { (self.0).$ctcifX(); self }
            fn global(&mut self) -> &mut Self { (self.0).$cgifX(); self }
        }
    };
    ($name:ident, $isrname:ident, $fcwname:ident, $CCR:ident, $sel:ident {
        $ptr:path, $dmaX:ident, $ccrX:ident, $cndtrX:ident, $cparX:ident, $cmarX:ident, $cXs:ident, $teifX:ident, $htifX:ident, $tcifX:ident, $gifX:ident, $cteifX:ident, $chtifX:ident, $ctcifX:ident, $cgifX:ident
    }) => {
        impl_channel!($name, $isrname, $fcwname, $CCR, $sel {
            $ptr, $ccrX, $cndtrX, $cparX, $cmarX, $cXs, $teifX, $htifX, $tcifX, $gifX, $cteifX, $chtifX, $ctcifX, $cgifX
        });
        impl ConfigurationRegister for stm32l4x2::$dmaX::$CCR {
            type W = stm32l4x2::$dmaX::$ccrX::W;
            // type R = stm32l4x2::$dmaX::$ccrX::R;
            fn reset(&self) { self.reset() }
            fn write<F>(&self, f: F) where F: FnOnce(&mut Self::W) -> &mut Self::W { self.write(f) }
        }
        impl ConfigurationRegisterWrite for stm32l4x2::$dmaX::$ccrX::W {
            fn minc(&mut self, v: bool) -> &mut Self { self.minc(v) }
            fn circ(&mut self, v: bool) -> &mut Self { self.circ(v) }
            fn dir(&mut self, v: bool) -> &mut Self { self.dir(v) }
            fn teie(&mut self, v: bool) -> &mut Self { self.teie(v) }
            fn htie(&mut self, v: bool) -> &mut Self { self.htie(v) }
            fn tcie(&mut self, v: bool) -> &mut Self { self.tcie(v) }
            fn en(&mut self, v: bool) -> &mut Self { self.en(v) }
        }
    };
}

impl_channel!(DMA1Channel7, DMA1Channel7ISR, DMA1Channel7FCW, CCR7, DMA1Channel7Selection {
    stm32l4x2::DMA1::ptr, dma1, ccr7, cndtr7, cpar7, cmar7, c7s, teif7, htif7, tcif7, gif7, set_cteif7, set_chtif7, set_ctcif7, set_cgif7
});
#[allow(non_camel_case_types)]
pub enum DMA1Channel7Selection {
    USART2_TX = 0b0010,
    I2C1_RX = 0b0011,
    TIM2 = 0b0100,
    TIM1 = 0b0111,
}

impl_channel!(DMA1Channel6, DMA1Channel6ISR, DMA1Channel6FCW, CCR6, DMA1Channel6Selection {
    stm32l4x2::DMA1::ptr, dma1, ccr6, cndtr6, cpar6, cmar6, c6s, teif6, htif6, tcif6, gif6, set_cteif6, set_chtif6, set_ctcif6, set_cgif6
});
#[allow(non_camel_case_types)]
pub enum DMA1Channel6Selection {
    USART2_RX = 0b0010,
    I2C1_TX = 0b0011,
    TIM16 = 0b0100,
    TIM3 = 0b0101, // Available on STM32L45xxx and STM32L46xxx devices only.
    TIM1 = 0b0111,
}

impl_channel!(DMA1Channel5, DMA1Channel5ISR, DMA1Channel5FCW, CCR5, DMA1Channel5Selection {
    stm32l4x2::DMA1::ptr, dma1, ccr5, cndtr5, cpar5, cmar5, c5s, teif5, htif5, tcif5, gif5, set_cteif5, set_chtif5, set_ctcif5, set_cgif5
});
#[allow(non_camel_case_types)]
pub enum DMA1Channel5Selection {
    SPI2_TX = 0b0001,
    USART1_RX = 0b0010,
    I2C2_RX = 0b0011,
    TIM2 = 0b0100,
    QUADSPI = 0b0101,
    TIM15 = 0b0111,
}

impl_channel!(DMA1Channel4, DMA1Channel4ISR, DMA1Channel4FCW, CCR4, DMA1Channel4Selection {
    stm32l4x2::DMA1::ptr, dma1, ccr4, cndtr4, cpar4, cmar4, c4s, teif4, htif4, tcif4, gif4, set_cteif4, set_chtif4, set_ctcif4, set_cgif4
});
#[allow(non_camel_case_types)]
pub enum DMA1Channel4Selection {
    SPI2_RX = 0b0001,
    USART1_TX = 0b0010,
    I2C2_TX = 0b0011,
    TIM7_DAC2 = 0b0101, // Available on STM32L43xxx and STM32L44xxx devices only.
    TIM1 = 0b0111,
}

impl_channel!(DMA1Channel3, DMA1Channel3ISR, DMA1Channel3FCW, CCR3, DMA1Channel3Selection {
    stm32l4x2::DMA1::ptr, dma1, ccr3, cndtr3, cpar3, cmar3, c3s, teif3, htif3, tcif3, gif3, set_cteif3, set_chtif3, set_ctcif3, set_cgif3
});
#[allow(non_camel_case_types)]
pub enum DMA1Channel3Selection {
    SPI1_TX = 0b0001,
    USART3_RX = 0b0010,
    I2C3_RX = 0b0011,
    TIM16 = 0b0100,
    TIM3 = 0b0101, // Available on STM32L45xxx and STM32L46xxx devices only.
    TIM6_DAC1 = 0b0110,
    TIM1 = 0b0111,
}

impl_channel!(DMA1Channel2, DMA1Channel2ISR, DMA1Channel2FCW, CCR2, DMA1Channel2Selection {
    stm32l4x2::DMA1::ptr, dma1, ccr2, cndtr2, cpar2, cmar2, c2s, teif2, htif2, tcif2, gif2, set_cteif2, set_chtif2, set_ctcif2, set_cgif2
});
#[allow(non_camel_case_types)]
pub enum DMA1Channel2Selection {
    SPI1_RX = 0b0001,
    USART3_TX = 0b0010,
    I2C3_TX = 0b0011,
    TIM2 = 0b0100,
    TIM3 = 0b0101, // Available on STM32L45xxx and STM32L46xxx devices only.
    TIM1 = 0b0111,
}
impl_channel!(DMA1Channel1, DMA1Channel1ISR, DMA1Channel1FCW, CCR1, DMA1Channel1Selection {
    stm32l4x2::DMA1::ptr, dma1, ccr1, cndtr1, cpar1, cmar1, c1s, teif1, htif1, tcif1, gif1, set_cteif1, set_chtif1, set_ctcif1, set_cgif1
});
#[allow(non_camel_case_types)]
pub enum DMA1Channel1Selection {
    ADC1 = 0b0000,
    TIM2 = 0b0100,
}

impl_channel!(DMA2Channel7, DMA2Channel7ISR, DMA2Channel7FCW, CCR7, DMA2Channel7Selection {
    stm32l4x2::DMA2::ptr, ccr7, cndtr7, cpar7, cmar7, c7s, teif7, htif7, tcif7, gif7, set_cteif7, set_chtif7, set_ctcif7, set_cgif7
});
#[allow(non_camel_case_types)]
pub enum DMA2Channel7Selection {
    SAI1_B = 0b0001,
    USART1_RX = 0b0010,
    QUADSPI = 0b0011,
    LPUART1_RX = 0b0100,
    I2C1_TX = 0b0101,
}

impl_channel!(DMA2Channel6, DMA2Channel6ISR, DMA2Channel6FCW, CCR6, DMA2Channel6Selection {
    stm32l4x2::DMA2::ptr, ccr6, cndtr6, cpar6, cmar6, c6s, teif6, htif6, tcif6, gif6, set_cteif6, set_chtif6, set_ctcif6, set_cgif6
});
#[allow(non_camel_case_types)]
pub enum DMA2Channel6Selection {
    SAI1_A = 0b0001,
    USART1_TX = 0b0010,
    LPUART1_TX = 0b0100,
    I2C1_RX = 0b0101,
}

impl_channel!(DMA2Channel5, DMA2Channel5ISR, DMA2Channel5FCW, CCR5, DMA2Channel5Selection {
    stm32l4x2::DMA2::ptr, ccr5, cndtr5, cpar5, cmar5, c5s, teif5, htif5, tcif5, gif5, set_cteif5, set_chtif5, set_ctcif5, set_cgif5
});
#[allow(non_camel_case_types)]
pub enum DMA2Channel5Selection {
    UART4_RX = 0b0010, // Available on STM32L45xxx and STM32L46xxx devices only.
    TIM7_DAC2 = 0b0011, // Available on STM32L43xxx and STM32L44xxx devices only.
    AES_IN = 0b0110, // Available on STM32L44xxx and STM32L46xxx devices only.
    SDMMC1 = 0b0111,
}

impl_channel!(DMA2Channel4, DMA2Channel4ISR, DMA2Channel4FCW, CCR4, DMA2Channel4Selection {
    stm32l4x2::DMA2::ptr, ccr4, cndtr4, cpar4, cmar4, c4s, teif4, htif4, tcif4, gif4, set_cteif4, set_chtif4, set_ctcif4, set_cgif4
});
#[allow(non_camel_case_types)]
pub enum DMA2Channel4Selection {
    TIM6_DAC1 = 0b0011,
    SPI1_TX = 0b0100,
    SDMMC1 = 0b0111,
}

impl_channel!(DMA2Channel3, DMA2Channel3ISR, DMA2Channel3FCW, CCR3, DMA2Channel3Selection {
    stm32l4x2::DMA2::ptr, ccr3, cndtr3, cpar3, cmar3, c3s, teif3, htif3, tcif3, gif3, set_cteif3, set_chtif3, set_ctcif3, set_cgif3
});
#[allow(non_camel_case_types)]
pub enum DMA2Channel3Selection {
    ADC1 = 0b0000,
    UART4_TX = 0b0010, // Available on STM32L45xxx and STM32L46xxx devices only.
    SPI1_RX = 0b0100,
    AES_OUT = 0b0110, // Available on STM32L44xxx and STM32L46xxx devices only.
}

impl_channel!(DMA2Channel2, DMA2Channel2ISR, DMA2Channel2FCW, CCR2, DMA2Channel2Selection {
    stm32l4x2::DMA2::ptr, ccr2, cndtr2, cpar2, cmar2, c2s, teif2, htif2, tcif2, gif2, set_cteif2, set_chtif2, set_ctcif2, set_cgif2
});
#[allow(non_camel_case_types)]
pub enum DMA2Channel2Selection {
    I2C4_TX = 0b0000, // Available on STM32L45xxx and STM32L46xxx devices only.
    SAI1_B = 0b0001,
    SPI3_TX = 0b0011,
    SWPMI1_TX = 0b0100, // Available on STM32L43xxx and STM32L44xxx devices only.
    AES_OUT = 0b0110, // Available on STM32L44xxx and STM32L46xxx devices only.
}

impl_channel!(DMA2Channel1, DMA2Channel1ISR, DMA2Channel1FCW, CCR1, DMA2Channel1Selection {
    stm32l4x2::DMA2::ptr, ccr1, cndtr1, cpar1, cmar1, c1s, teif1, htif1, tcif1, gif1, set_cteif1, set_chtif1, set_ctcif1, set_cgif1
});
#[allow(non_camel_case_types)]
pub enum DMA2Channel1Selection {
    I2C4_RX = 0b0000, // Available on STM32L45xxx and STM32L46xxx devices only.
    SAI1_A = 0b0001,
    SPI3_RX = 0b0011,
    SWPMI1_RX = 0b0100, // Available on STM32L43xxx and STM32L44xxx devices only.
    AES_IN = 0b0110, // Available on STM32L44xxx and STM32L46xxx devices only.
}