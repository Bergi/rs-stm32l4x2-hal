//! LCD module

use ::stm32l4x2;

use ::power::{
    Power
};
use ::rcc::{
    BDCR,
    AHB,
    APB1,
    RtcClockType
};

use ::mem;

pub mod config;
pub mod ram;

pub enum ValidationResult {
    ///Valid Frame Rate
    ///
    ///Contains approximate frame rate
    Ok(u32),
    ///RTC clock is not set. Refer to `rcc::BDCR` for how to set.
    ClockNotSet,
    ///Resulting frame rate is outside of range is below minimum ~30Hz
    SmallFrameRate,
    ///Resulting frame rate is outside of range is is above ~100Hz
    BigFrameRate
}

///LCD representations that provides access to HW LCD
///
///Implements destructor that turns off LCD.
pub struct LCD {
    inner: stm32l4x2::LCD
}

#[inline]
fn calculate_frame_rate(clock_frequency: u32, ps: u32, div: u32, duty: u8) -> u32 {
    //Take duty * 1000, then divide by 1000 to drop floating point part
    (clock_frequency / ((2u32.pow(ps)) * (16 + div)) * match duty {
        0 => 1000,
        1 => 500,
        2 => 333,
        3 => 250,
        4 => 125,
        _ => unreachable!(),
    } / 1000)
}

impl LCD {
    ///Initializes HW for LCD with LSE as clock source
    ///
    ///## Steps:
    ///
    ///1. Enable peripheral clocks
    ///2. Set LSE as RTC clock.
    ///3. Turn on LCD's clock
    pub fn init_lse(apb1: &mut APB1, ahb: &mut AHB, pwr: &mut Power, bdcr: &mut BDCR) {
        //Enables peripheral clocks
        apb1.enr1().modify(|_, w| w.set_pwren());
        //Enables LCD GPIO
        ahb.enr2().modify(|_, w| {
            w.set_gpioaen();
            w.set_gpioben();
            w.set_gpiocen();
            w.set_gpioden()
        });

        //Configures RTC clock
        pwr.remove_bdp();
        //TODO: Reset BDCR to change clock?
        bdcr.lse_enable(true);
        bdcr.set_rtc_clock(RtcClockType::LSE);

        //Turn LCD's clock
        apb1.enr1().modify(|_, w| w.set_lcden());
    }

    ///Initializes LCD
    ///
    ///## Requirements
    ///
    /// * Pre-initialize HW using `init` method.
    /// * Configure LCD GPIO pins as alternative functions. You should check board's documentation
    /// for pins.
    /// * Verify configuration through `validate` function
    ///
    ///## Steps:
    ///
    ///1. Turns off.
    ///2. Reset RAM registers and set update request.
    ///3. Performs configuration.
    ///4. Turns on.
    pub fn new(lcd: stm32l4x2::LCD, config: config::Config) -> Self {
        let mut lcd = Self {
            inner: lcd
        };

        lcd.off();

        lcd.reset_ram();
        lcd.update_request();

        lcd.configure(config);

        lcd.on();

        //Wait for LCD to get enabled
        while !lcd.inner.sr.read().ens() {}
        //Wait for LCD to get ready
        while !lcd.inner.sr.read().rdy() {}

        lcd
    }

    ///Performs validation of settings.
    ///
    ///HSE clock is not supported yet...
    pub fn validate(lcd: &mut stm32l4x2::LCD, bdcr: &mut BDCR, configuration: &config::Config) -> ValidationResult {
        let clock_frequency: u32 = match bdcr.rtc_clock() {
            RtcClockType::None => return ValidationResult::ClockNotSet,
            RtcClockType::LSE => 32_000,
            RtcClockType::LSI => 32_768,
            RtcClockType::HSE => panic!("HSE clock is not supported yet")
        };

        let ps = configuration.prescaler.as_ref().map(|val| *val as u8).unwrap_or(lcd.fcr.read().ps()) as u32;
        let div = configuration.divider.as_ref().map(|val| *val as u8).unwrap_or(lcd.fcr.read().div()) as u32;
        let duty = configuration.duty.as_ref().map(|val| *val as u8).unwrap_or(lcd.cr.read().duty());

        let frame_rate = calculate_frame_rate(clock_frequency, ps, div, duty);

        if frame_rate < 29 {
            ValidationResult::SmallFrameRate
        } else if frame_rate > 110 {
            ValidationResult::BigFrameRate
        } else {
            ValidationResult::Ok(frame_rate)
        }
    }

    #[inline]
    ///Returns whether LCD is enabled or not
    pub fn is_enabled(&mut self) -> bool {
        self.inner.sr.read().ens()
    }

    #[inline]
    ///Returns whether LCD is ready or not
    pub fn is_ready(&mut self) -> bool {
        self.inner.sr.read().rdy()
    }

    ///Performs LCD's configuration
    pub fn configure(&mut self, config: config::Config) {
        let config::Config{
            prescaler, divider, blink_mode, blink_freq, contrast, dead_time, pulse_duration, high_drive,
            bias, duty, mux_segment, voltage_source} = config;

        self.inner.fcr.modify(|_, w| {
            if let Some(prescaler) = prescaler {
                unsafe { w.ps(prescaler as u8); }
            }
            if let Some(div) = divider {
                unsafe { w.div(div as u8); }
            }
            if let Some(blink) = blink_mode {
                unsafe { w.blink(blink as u8); }
            }
            if let Some(blinkf) = blink_freq {
                unsafe { w.blinkf(blinkf as u8); }
            }
            if let Some(contrast) = contrast {
                unsafe { w.cc(contrast as u8); }
            }
            if let Some(dead) = dead_time {
                unsafe { w.dead(dead as u8); }
            }
            if let Some(pulse) = pulse_duration {
                unsafe { w.pon(pulse as u8); }
            }
            match high_drive {
                Some(config::HighDrive::On) => w.set_hd(),
                Some(config::HighDrive::Off) => w.clear_hd(),
                _ => w
            }
        });

        //Wait for FCR to sync
        while !self.inner.sr.read().fcrsf() {}

        self.inner.cr.modify(|_, w| {
            if let Some(bias) = bias {
                unsafe { w.bias(bias as u8); }
            }
            if let Some(duty) = duty {
                unsafe { w.duty(duty as u8); }
            }
            match voltage_source {
                Some(config::VoltageSource::Internal) => w.set_vsel(),
                Some(config::VoltageSource::External) => w.clear_vsel(),
                _ => w
            };
            match mux_segment {
                Some(config::MuxSegment::On) => w.set_mux_seg(),
                Some(config::MuxSegment::Off) => w.clear_mux_seg(),
                _ => w
            }
        });
    }

    #[inline]
    ///Resets LCD's RAM.
    ///
    ///To have effect user must request update.
    pub fn reset_ram(&mut self) {
        self.inner.ram_com0.reset();
        self.inner.ram_com1.reset();
        self.inner.ram_com2.reset();
        self.inner.ram_com3.reset();
        self.inner.ram_com4.reset();
        self.inner.ram_com5.reset();
        self.inner.ram_com6.reset();
        self.inner.ram_com7.reset();
    }

    #[inline]
    ///Requests to transfer written data to buffer by setting SR's UDR bit
    ///
    ///Note: Once set, it can be cleared only by hardware
    ///In addition to that until value is cleared, RAM is write-protected.
    ///
    ///No update can occur until display shall be enabled.
    pub fn update_request(&mut self) {
        self.inner.sr.modify(|_, w| w.set_udr())
    }

    #[inline]
    ///Turns LCD on by setting CR's EN bit
    pub fn on(&mut self) {
        self.inner.cr.modify(|_, w| w.set_lcden())
    }

    #[inline]
    ///Turns LCD off by clearing CR's EN bit
    pub fn off(&mut self) {
        self.inner.cr.modify(|_, w| w.set_lcden())
    }

    /// Starts listening for an `event`
    pub fn subscribe(&mut self, event: config::Event) {
        self.inner.fcr.modify(|_, w| match event {
            config::Event::StartFrame => w.set_sofie(),
            config::Event::UpdateDone => w.set_uddie(),
        })
    }

    /// Stops listening for an `event`
    pub fn unsubscribe(&mut self, event: config::Event) {
        self.inner.fcr.modify(|_, w| match event {
            config::Event::StartFrame => w.clear_sofie(),
            config::Event::UpdateDone => w.clear_uddie(),
        })
    }

    /// Writes into RAM by index.
    pub fn write_ram<I: self::ram::Index>(&mut self, data: u32) {
        I::write(self, data)
    }

    pub fn into_raw(mut self) -> stm32l4x2::LCD {
        //We cannot move out of value that implements Drop
        //so let's trick it and since underlying LCD doesn't implement Drop it is safe.
        let mut result = unsafe { mem::uninitialized::<stm32l4x2::LCD>()};
        mem::swap(&mut result, &mut self.inner);
        mem::forget(self);

        result
    }
}

impl Drop for LCD {
    fn drop(&mut self) {
        self.off();
    }
}

#[cfg(test)]
mod tests {
    #[test]
    pub fn calculate_frame_rate() {
        use super::config;

        //Reference manual Ch. 25.3.2 Table 160
        let frame_rate = super::calculate_frame_rate(32_768, config::Prescaler::PS_8 as u32, config::Divider::DIV_17 as u32, config::Duty::OneTo8 as u8);
        assert_eq!(frame_rate, 30);
        let frame_rate = super::calculate_frame_rate(32_768, 4, 1, config::Duty::OneTo4 as u8);
        assert_eq!(frame_rate, 30);
        let frame_rate = super::calculate_frame_rate(32_768, 4, 6, config::Duty::OneTo3 as u8);
        assert_eq!(frame_rate, 30);
        let frame_rate = super::calculate_frame_rate(32_768, 4, 6, config::Duty::OneTo3 as u8);
        assert_eq!(frame_rate, 30);
        let frame_rate = super::calculate_frame_rate(32_768, 5, 1, config::Duty::OneTo2 as u8);
        assert_eq!(frame_rate, 30);
        let frame_rate = super::calculate_frame_rate(32_768, config::Prescaler::PS_64 as u32, config::Divider::DIV_17 as u32, config::Duty::Static as u8);
        assert_eq!(frame_rate, 30);

        let frame_rate = super::calculate_frame_rate(32_768, 1, 4, config::Duty::OneTo8 as u8);
        assert_eq!(frame_rate, 102);
        let frame_rate = super::calculate_frame_rate(32_768, 2, 4, config::Duty::OneTo4 as u8);
        assert_eq!(frame_rate, 102);
        let frame_rate = super::calculate_frame_rate(32_768, 2, 11, config::Duty::OneTo3 as u8);
        assert_eq!(frame_rate, 100);
        let frame_rate = super::calculate_frame_rate(32_768, 4, 4, config::Duty::Static as u8);
        assert_eq!(frame_rate, 102);

        let frame_rate = super::calculate_frame_rate(1_000_000, 6, 3, config::Duty::OneTo8 as u8);
        assert_eq!(frame_rate, 102);
        let frame_rate = super::calculate_frame_rate(1_000_000, 8, 3, config::Duty::OneTo2 as u8);
        assert_eq!(frame_rate, 102);
    }
}
