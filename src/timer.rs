//! Hardware Timers
use cortex_m::peripheral::SYST;
use cortex_m::peripheral::syst::SystClkSource;
use hal::timer::{CountDown, Periodic};
use gencos::Awaitable;
use nb;

use config::SYST_MAX_RVR;
use time::Hertz;
use rcc::{APB1, APB2, Clocks};

use stm32l4x2::{
    //advanced timers
    TIM1, //stm32l4x2::rcc::apb2enr | apb2rstr:
    //General purpose
    TIM2, //stm32l4x2::rcc::apb1enr1 | apb1rstr1
    TIM15, TIM16, //stm32l4x2::rcc::apb2enr | apb2rstr
    //Basic timers
    TIM6, TIM7, //stm32l4x2::rcc::apb1enr1 | apb1rstr1
    // low-power timer
    //LPTIM1, LPTIM2, //stm32l4x2::rcc::apb1enr1 | apb1rstr1
};


///HW Timer
pub struct Timer<TIM> {
    tim: TIM,
    psc: Hertz,
}

impl Timer<SYST> {
    pub fn syst<T: Into<Hertz>>(mut syst: SYST, timeout: T, clocks: Clocks) -> Self {
        syst.set_clock_source(SystClkSource::Core);
        let mut timer = Timer { tim: syst, psc: clocks.sysclk };
        timer.start(timeout);
        timer
    }

    /// Starts listening for an `event`
    pub fn subscribe(&mut self) {
        self.tim.enable_interrupt();
    }

    /// Stops listening for an `event`
    pub fn unsubscribe(&mut self) {
        self.tim.disable_interrupt();
    }
}

impl CountDown for Timer<SYST> {
    type Time = Hertz;

    fn start<T: Into<Hertz>>(&mut self, timeout: T) {
        let rvr = self.psc / timeout - 1;

        assert!(rvr < SYST_MAX_RVR);

        self.tim.set_reload(rvr);
        self.tim.clear_current();
        self.tim.enable_counter();
    }

    fn wait(&mut self) -> nb::Result<(), !> {
        match self.tim.has_wrapped() {
            true => Ok(()),
            false => Err(nb::Error::WouldBlock)
        }
    }
}

///Type alias for timer based on system clock.
pub type Sys = Timer<SYST>;

macro_rules! impl_timer {
    ($($TIMx:ident: [alias: $Alias:ident; constructor: $timx:ident; $APB:ident: {apb: $apb:ident; $enr:ident: $enr_bit:ident; $rstr:ident: $rstr_bit:ident; ppre: $ppre:ident}])+) => {
        $(
            ///Type alias for TIM timer.
            pub type $Alias = Timer<$TIMx>;

            impl Timer<$TIMx> {
                ///Creates new instance of timer.
                pub fn $timx<F: Into<Hertz>>(tim: $TIMx, psc_freq: F, clocks: Clocks, apb: &mut $APB) -> Self {
                    // enable peripheral
                    apb.$enr().modify(|_, w| w.$enr_bit(true));
                    // reset to a clean slate state
                    // apb.$rstr().modify(|_, w| w.$rstr_bit(true));
                    // apb.$rstr().modify(|_, w| w.$rstr_bit(false));

                    tim.cr1.write(|w| w
                        .arpe(false) // do not cache arr
                        .opm(false) // do not run in one-pulse-mode
                        .urs(true) // only counter overflow generates interrupt/DMA request
                        .cen(false) // counter disabled
                    );
                    let prescaler_ratio = clocks.$apb / psc_freq;
                    // TODO: TIM2 supports 32 bit
                    debug_assert!(prescaler_ratio <= 1 << 16);
                    // use core::convert::TryFrom;
                    // debug_assert!(<u16>::try_from(prescaler_ratio).is_ok());
                    tim.psc.write(|w| unsafe { w.psc(prescaler_ratio as u16 - 1) });
                    // trigger update event
                    // to reset (re-initialize) the timer counter and prescaler counter
                    // take psc and arr into shadow registers
                    // does not cause interrupt/dma because of urs=1
                    tim.egr.write(|w| w.set_ug()); // clears itself
                    debug_assert!(tim.arr.read().bits() != 0); // ???
                    debug_assert!(!tim.sr.read().uif());
                    debug_assert!(tim.cnt.read().bits() == 0);

                    Timer {
                        tim,
                        psc: clocks.pclk1 / prescaler_ratio
                    }
                }
                pub fn set_autoreload<F: Into<Hertz>>(&mut self, freq: F) {
                    let period = (self.psc / freq) as u16;
                    debug_assert!(period > 1); // counter is blocked otherwise (when period-1 = 0)
                    self.tim.arr.write(|w| unsafe { w.arr(period - 1) });
                }
                pub fn start<F: Into<Hertz>>(&mut self, freq: F) {
                    self.set_autoreload(freq);
                    // FIXME: use modify? write is ok for TIM6/7
                    self.tim.cr1.write(|w| w
                        .arpe(false) // do not cache arr
                        .opm(false) // do not run in one-pulse-mode
                        .urs(false) // any update event causes interrupt
                        .cen(true) // counter enabled
                    );
                }

                fn update_interrupt(&self, enabled: bool) {
                    // FIXME: Clears everything else (DMA request etc)!
                    self.tim.dier.write(|w| w.uie(enabled));
                }
                /// Starts listening for an `event`
                pub fn subscribe(&mut self) {
                    self.update_interrupt(true);
                }
                /// Stops listening for an `event`
                pub fn unsubscribe(&mut self) {
                    self.update_interrupt(false);
                }

                #[inline(always)]
                /// Resets SR's UIF register to clear status of overflow.
                ///
                /// Unless reset is done, Interrupt handler is going to be continiously called.
                #[inline(always)]
                pub fn clear_update_event(&mut self) {
                    // FIXME: keep other event flags, reset() is only ok for TIM6/7
                    // self.tim.sr.modify(|_, w| w.uif().clear_bit());
                    self.tim.sr.reset(); // clear uif
                }

                /// Paused timer and releases the TIM peripheral
                pub fn free(self) -> $TIMx {
                    // FIXME: apb.$enr().modify(|_, w| w.$enr_bit().clear_bit());
                    self.tim.cr1.modify(|_, w| w.clear_cen());
                    self.tim
                }
            }

            impl Periodic for Timer<$TIMx> {}
            impl CountDown for Timer<$TIMx> {
                type Time = Hertz;
                fn start<T: Into<Self::Time>>(&mut self, timeout: T) {
                    self.set_autoreload(timeout);
                    self.tim.cr1.write(|w| w
                        .arpe(false) // do not cache arr
                        .opm(false) // do not run in one-pulse-mode
                        .urs(false) // any update event causes interrupt
                        .cen(true) // counter enabled
                    );
                }

                fn wait(&mut self) -> nb::Result<(), !> {
                    if !self.tim.sr.read().uif() {
                        Err(nb::Error::WouldBlock)
                    } else {
                        self.clear_update_event();
                        Ok(())
                    }
                }
            }

            impl Awaitable for Timer<$TIMx> {
                type Waker = ::gencos::runner::GlobalWaker;
                fn is_ready(&self) -> bool {
                    self.tim.sr.read().uif()
                }
                fn set_wakeup(&self, _:Self::Waker) {
                    self.update_interrupt(true);
                }
            }
        )+
    }
}

impl_timer!(
    TIM1: [
        alias: Tim1;
        constructor: tim1;
        APB2: {
            apb: pclk2;
            enr: tim1en;
            rstr: tim1rst;
            ppre: ppre2
        }
    ]
    /* TODO: support 32 bit
    TIM2: [
        alias: Tim2;
        constructor: tim2;
        APB1: {
            apb: pclk1;
            enr1: tim2en;
            rstr1: tim2rst;
            ppre: ppre1
        }
    ] */
    TIM15: [
        alias: Tim15;
        constructor: tim15;
        APB2: {
            apb: pclk2;
            enr: tim15en;
            rstr: tim15rst;
            ppre: ppre2
        }
    ]
    TIM16: [
        alias: Tim16;
        constructor: tim16;
        APB2: {
            apb: pclk2;
            enr: tim16en;
            rstr: tim16rst;
            ppre: ppre2
        }
    ]
    TIM6: [
        alias: Tim6;
        constructor: tim6;
        APB1: {
            apb: pclk1;
            enr1: tim6en;
            rstr1: tim6rst;
            ppre: ppre1
        }
    ]
    TIM7: [
        alias: Tim7;
        constructor: tim7;
        APB1: {
            apb: pclk1;
            enr1: tim7en;
            rstr1: tim7rst;
            ppre: ppre1
        }
    ]
);
